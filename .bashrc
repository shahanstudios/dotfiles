# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Directory Configs
export ANDROID_HOME="$XDG_DATA_HOME"/android
export AWS_SHARED_CREDENTIALS_FILE="$XDG_CONFIG_HOME"/aws/credentials
export AWS_CONFIG_FILE="$XDG_CONFIG_HOME"/aws/config
export RUSTUP_HOME="$XDG_CONFIG_HOME"/rustup
export CARGO_HOME="$XDG_DATA_HOME"/cargo
export CUDA_CACHE_PATH="$XDG_CACHE_HOME"/nv
export __GL_SHADER_DISK_CACHE_PATH="$XDG_CACHE_HOME"/nv
export GNUPGHOME="$XDG_DATA_HOME"/gnupg
export GRADLE_USER_HOME="$XDG_DATA_HOME"/gradle
export GTK2_RC_FILES="$XDG_CONFIG_HOME"/gtk-2.0/gtkrc
export ICEAUTHORITY="$XDG_CACHE_HOME"/ICEauthority
export NUGET_PACKAGES="$XDG_CACHE_HOME"/NuGetPackages
export PLATFORMIO_CORE_DIR="$XDG_DATA_HOME"/platformio
export WINEPREFIX="$XDG_DATA_HOME"/wine

# alias ls='ls -l --color=auto --group-directories-first'
alias ls='exa --icons --group-directories-first -l'
alias config="/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME"
alias celluloid='celluloid --new-window'
alias wget=wget --hsts-file="$XDG_DATA_HOME/wget-hsts"

# PS1='[\u@\h \W]\$ '
eval "$(starship init bash)"
HISTFILE="$HOME/.local/.bash_history"
export HISTCONTROL=ignoreboth

export MSBuildSDKsPath=$( echo /usr/share/dotnet/sdk/7.*/Sdks );
export DOTNET_CLI_TELEMETRY_OPTOUT=1

alias rm='rmtrash'
alias rmdir='rmdirtrash'
alias vim='nvim'
alias pdflatex='pdflatex -halt-on-error'

export _JAVA_AWT_WM_NONREPARENTIN=1

# BEGIN_KITTY_SHELL_INTEGRATION
if test -n "$KITTY_INSTALLATION_DIR" -a -e "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; then source "$KITTY_INSTALLATION_DIR/shell-integration/bash/kitty.bash"; fi
# END_KITTY_SHELL_INTEGRATION
