local lsp_plugins = require('shahanstudios/plugins/lsp')
local completion_plugins = require('shahanstudios/plugins/completion')
local telescope_plugins = require('shahanstudios/plugins/telescope')
local treesitter_plugins = require('shahanstudios/plugins/treesitter')

require('lazy').setup({
	'tpope/vim-fugitive',

	-- Detect tabstop and shiftwidth automatically
	-- 'tpope/vim-sleuth',

	lsp_plugins,
	completion_plugins,
	telescope_plugins,
	treesitter_plugins,

	-- Useful plugin to show you pending keybinds.
	{ 'folke/which-key.nvim', opts = {} },
	-- {
	-- 	'oxfist/night-owl.nvim',
	-- 	lazy = false,
	-- 	priority = 1000,
	-- 	config = function()
	-- 		vim.cmd.colorscheme('night-owl')
	-- 	end,
	-- },
	{
		-- Set lualine as statusline
		'nvim-lualine/lualine.nvim',
		-- See `:help lualine.txt`
		opts = {
			options = {
				component_separators = '|',
				section_separators = '',
			},
		},
	},
	-- {
	-- 	-- Add indentation guides even on blank lines
	-- 	'lukas-reineke/indent-blankline.nvim',
	-- 	-- Enable `lukas-reineke/indent-blankline.nvim`
	-- 	-- See `:help ibl`
	-- 	main = 'ibl',
	-- 	opts = {},
	-- },
	{ 'numToStr/Comment.nvim', opts = {} },
	{
		'catppuccin/nvim',
		name = 'catppuccin',
		priority = 1000,
		config = function()
		    vim.cmd.colorscheme("catppuccin-mocha")
		end,
	}
}, {})
