local status_ok, lspconfig = pcall(require, "lspconfig");
if not status_ok then
 return
end
-- 
-- local custom_attach = function(client)
-- end
-- 
-- local setup_server = function(server, config)
	-- if not config then
		-- return
	-- end
-- 
	-- if type(config) ~= "table" then
		-- config = {}
	-- end
-- 
	-- config = vim.tbl_deep_extend("force", {
		-- on_attach = function(client) 
			-- print(server, 'LSP Attached')
			-- custom_attach(client)
		-- end,
		-- capabilities = updated_capabilities
	-- }, config)
-- 
	-- lspconfig[server].setup(config)
-- end
-- 
-- local servers = {
	-- tsserver = true,
	-- csharp_ls = true,
	-- vimls = true,
	-- bashls = true,
	-- clangd = true,
	-- html = true,
	-- cssls = true,
	-- astro = true,
	-- -- rust_analyzer = true,
	-- gdscript = true,
-- }
-- 
-- for server, config in pairs(servers) do
	-- setup_server(server, config)
-- end
-- 
-- lspconfig.rust_analyzer.setup{}
-- 
-- local setup_omnisharp = function(server, config)
	-- if not config then
		-- return
	-- end
-- 
	-- if type(config) ~= "table" then
		-- config = {}
	-- end
-- 
	-- config = vim.tbl_deep_extend("force", {
		-- on_attach = function(client) 
			-- print(server, 'LSP Attached')
			-- custom_attach(client)
		-- end,
		-- cmd = { "dotnet", "/path/to/omnisharp/OmniSharp.dll" },
-- 
		-- -- Enables support for reading code style, naming convention and analyzer
		-- -- settings from .editorconfig.
		-- enable_editorconfig_support = true,
-- 
		-- -- If true, MSBuild project system will only load projects for files that
		-- -- were opened in the editor. This setting is useful for big C# codebases
		-- -- and allows for faster initialization of code navigation features only
		-- -- for projects that are relevant to code that is being edited. With this
		-- -- setting enabled OmniSharp may load fewer projects and may thus display
		-- -- incomplete reference lists for symbols.
		-- enable_ms_build_load_projects_on_demand = false,
-- 
		-- -- Enables support for roslyn analyzers, code fixes and rulesets.
		-- enable_roslyn_analyzers = false,
-- 
		-- -- Specifies whether 'using' directives should be grouped and sorted during
		-- -- document formatting.
		-- organize_imports_on_format = false,
-- 
		-- -- Enables support for showing unimported types and unimported extension
		-- -- methods in completion lists. When committed, the appropriate using
		-- -- directive will be added at the top of the current file. This option can
		-- -- have a negative impact on initial completion responsiveness,
		-- -- particularly for the first few completion sessions after opening a
		-- -- solution.
		-- enable_import_completion = false,
-- 
		-- -- Specifies whether to include preview versions of the .NET SDK when
		-- -- determining which version to use for project loading.
		-- sdk_include_prereleases = true,
-- 
		-- -- Only run analyzers against open files when 'enableRoslynAnalyzers' is
		-- -- true
		-- analyze_open_documents_only = false,
		-- capabilities = updated_capabilities
	-- }, config)
-- 
	-- lspconfig[server].setup(config)
-- end
-- 
-- -- setup_omnisharp('omnisharp', true)







-- [[ Configure LSP ]]
--  This function gets run when an LSP connects to a particular buffer.
local on_attach = function(_, bufnr)
  -- NOTE: Remember that lua is a real programming language, and as such it is possible
  -- to define small helper and utility functions so you don't have to repeat yourself
  -- many times.
  --
  -- In this case, we create a function that lets us more easily define mappings specific
  -- for LSP related items. It sets the mode, buffer and description for us each time.
  local nmap = function(keys, func, desc)
    if desc then
      desc = 'LSP: ' .. desc
    end

    vim.keymap.set('n', keys, func, { buffer = bufnr, desc = desc })
  end

  nmap('<leader>rn', vim.lsp.buf.rename, '[R]e[n]ame')
  nmap('<leader>ca', vim.lsp.buf.code_action, '[C]ode [A]ction')

  nmap('gd', require('telescope.builtin').lsp_definitions, '[G]oto [D]efinition')
  nmap('gr', require('telescope.builtin').lsp_references, '[G]oto [R]eferences')
  nmap('gI', require('telescope.builtin').lsp_implementations, '[G]oto [I]mplementation')
  nmap('<leader>D', require('telescope.builtin').lsp_type_definitions, 'Type [D]efinition')
  nmap('<leader>ds', require('telescope.builtin').lsp_document_symbols, '[D]ocument [S]ymbols')
  nmap('<leader>ws', require('telescope.builtin').lsp_dynamic_workspace_symbols, '[W]orkspace [S]ymbols')

  -- See `:help K` for why this keymap
  nmap('K', vim.lsp.buf.hover, 'Hover Documentation')
  nmap('<C-k>', vim.lsp.buf.signature_help, 'Signature Documentation')

  -- Lesser used LSP functionality
  nmap('gD', vim.lsp.buf.declaration, '[G]oto [D]eclaration')
  nmap('<leader>wa', vim.lsp.buf.add_workspace_folder, '[W]orkspace [A]dd Folder')
  nmap('<leader>wr', vim.lsp.buf.remove_workspace_folder, '[W]orkspace [R]emove Folder')
  nmap('<leader>wl', function()
    print(vim.inspect(vim.lsp.buf.list_workspace_folders()))
  end, '[W]orkspace [L]ist Folders')

  -- Create a command `:Format` local to the LSP buffer
  vim.api.nvim_buf_create_user_command(bufnr, 'Format', function(_)
    vim.lsp.buf.format()
  end, { desc = 'Format current buffer with LSP' })
end

-- document existing key chains
-- require('which-key').register {
--   ['<leader>c'] = { name = '[C]ode', _ = 'which_key_ignore' },
--   ['<leader>d'] = { name = '[D]ocument', _ = 'which_key_ignore' },
--   ['<leader>g'] = { name = '[G]it', _ = 'which_key_ignore' },
--   ['<leader>h'] = { name = 'Git [H]unk', _ = 'which_key_ignore' },
--   ['<leader>r'] = { name = '[R]ename', _ = 'which_key_ignore' },
--   ['<leader>s'] = { name = '[S]earch', _ = 'which_key_ignore' },
--   ['<leader>t'] = { name = '[T]oggle', _ = 'which_key_ignore' },
--   ['<leader>w'] = { name = '[W]orkspace', _ = 'which_key_ignore' },
-- }
-- -- register which-key VISUAL mode
-- -- required for visual <leader>hs (hunk stage) to work
-- require('which-key').register({
--   ['<leader>'] = { name = 'VISUAL <leader>' },
--   ['<leader>h'] = { 'Git [H]unk' },
-- }, { mode = 'v' })

-- mason-lspconfig requires that these setup functions are called in this order
-- before setting up the servers.
require('mason').setup()
require('mason-lspconfig').setup()

-- Enable the following language servers
--  Feel free to add/remove any LSPs that you want here. They will automatically be installed.
--
--  Add any additional override configuration in the following tables. They will be passed to
--  the `settings` field of the server config. You must look up that documentation yourself.
--
--  If you want to override the default filetypes that your language server will attach to you can
--  define the property 'filetypes' to the map in question.
local servers = {
  -- clangd = {},
  -- gopls = {},
  -- pyright = {},
  rust_analyzer = {},
  -- tsserver = {},
  -- html = { filetypes = { 'html', 'twig', 'hbs'} },

  lua_ls = {
    Lua = {
      workspace = { checkThirdParty = false },
      telemetry = { enable = false },
      -- NOTE: toggle below to ignore Lua_LS's noisy `missing-fields` warnings
      diagnostics = { disable = { 'missing-fields' } },
    },
  },
}

-- Setup neovim lua configuration
-- require('neodev').setup()

-- nvim-cmp supports additional completion capabilities, so broadcast that to servers
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Ensure the servers above are installed
local mason_lspconfig = require 'mason-lspconfig'
--
mason_lspconfig.setup {
	ensure_installed = vim.tbl_keys(servers),
}

mason_lspconfig.setup_handlers {
	function(server_name)
		require('lspconfig')[server_name].setup {
			capabilities = capabilities,
			on_attach = on_attach,
			settings = servers[server_name],
			filetypes = (servers[server_name] or {}).filetypes,
		}
	end,
}
