#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

WD="$(dirname "$0")"
LEFT_CONFIG="$WD/bars/leftStatusBarConfig"
RIGHT_CONFIG="$WD/bars/rightStatusBarConfig"

rm /tmp/ipc-rightBar
MONITOR=$(get_monitor_id "Right") polybar --config=${RIGHT_CONFIG} --reload statusBar 2>/tmp/polybar_rightBar_error &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-rightBar

rm /tmp/ipc-leftBar
MONITOR=$(get_monitor_id "Left") polybar --config=${LEFT_CONFIG} --reload statusBar 2>/tmp/polybar_leftBar_error &
ln -s /tmp/polybar_mqueue.$! /tmp/ipc-leftBar
