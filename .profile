export PATH=$HOME/.bin:$HOME/.bin/bspwm:$HOME/.local/bin:$PATH

export TERM='kitty';
export TERM_EDIT='kitty';
export EDITOR='nvim';

export XDG_CONFIG_HOME=$HOME/.config
export XDG_CACHE_HOME=$HOME/.cache
export XDG_DATA_HOME=$HOME/.local/share
export XDG_STATE_HOME=$HOME/.local/state

export X_FILES_DIR="$XDG_CONFIG_HOME"/X11
export XINITRC="$X_FILES_DIR"/xinitrc
export XSERVERRC="$X_FILES_DIR"/xserverrc
export XAUTHORITY="$XDG_RUNTIME_DIR"/Xauthority

# export QT_STYLE_OVERRIDE=kvantum
export QT_QPA_PLATFORMTHEME=qt5ct

export VDPAU_DRIVER=nvidia
export LIBVA_DRIVER_NAME=vdpau

if [ -z "$TMUX" ]; then
	xinit
fi
