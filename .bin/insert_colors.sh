#!/bin/bash

CFG=$1

BEGIN="### START COLORS"
END="### END COLORS"

BEGIN_ADD="/^${BEGIN}/"
END_ADD="/^${END}/"

sed -i "${BEGIN_ADD},${END_ADD}{${BEGIN_ADD}!{${END_ADD}!d;};}" "$CFG"

sed -i "${BEGIN_ADD} r /dev/stdin" "$CFG" 
